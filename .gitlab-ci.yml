# using remote to allow for local builds with gitlab-ci-local
include:
  - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/3d80a75e1284935e69219bfa80bc6064c6786819/templates/fedora.yml'
  - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/3d80a75e1284935e69219bfa80bc6064c6786819/templates/ubuntu.yml'
  - remote: 'https://gitlab.freedesktop.org/freedesktop/ci-templates/-/raw/3d80a75e1284935e69219bfa80bc6064c6786819/templates/arch.yml'

stages:
  - build_docker
  - build
  - test
  - release

variables:
  FDO_DISTRIBUTION_VERSION: latest
  FDO_UPSTREAM_REPO: '${CI_PROJECT_PATH}'
  FDO_DISTRIBUTION_TAG: 'built-on-2023-04-13-4'

# this is for general compilation/testing/day-to-day CI
# this is also for umr.pkg.tar.zst release
build_docker_arch:
  extends:
    - .fdo.container-build@arch
  stage: build_docker
  variables:
    FDO_DISTRIBUTION_PACKAGES: >
      binutils
      cmake
      fakeroot
      gcc
      git
      glew
      libdrm
      llvm
      make
      nanomsg
      sdl2

# build template
.build: &build
  extends:
    - .fdo.distribution-image@arch
  stage: build
  script:
    - cmake -G'Unix Makefiles' -B build $CMAKE_EXTRA_ARGS
    - make -C build -j $FDO_CI_CONCURRENT $MAKE_EXTRA_ARGS
  artifacts:
    paths:
    - build/src/app/umr

build:
  <<: *build

build_no_gui:
  variables:
    CMAKE_EXTRA_ARGS: '-DUMR_GUI=OFF -DUMR_SERVER=ON'
  <<: *build

build_no_gui_no_server:
  variables:
    CMAKE_EXTRA_ARGS: '-DUMR_GUI=OFF -DUMR_SERVER=OFF'
  <<: *build

test_help:
  extends:
    - .fdo.distribution-image@arch
  needs: [build]
  stage: test
  script:
    - ./build/src/app/umr -h
    # gui option present
    - bash -c "./build/src/app/umr -h | grep -F -- '--gui'"
    # server option present
    - bash -c "./build/src/app/umr -h | grep -F -- '--server'"

# sanity check for a gui-less build
test_help_no_gui:
  extends:
    - .fdo.distribution-image@arch
  needs: [build_no_gui]
  stage: test
  script:
    - ./build/src/app/umr -h
    # no gui option, invert result
    - bash -c "./build/src/app/umr -h | grep -F -- '--gui' && exit 1; exit 0"
    # server option present
    - bash -c "./build/src/app/umr -h | grep -F -- '--server'"

# sanity check for a gui-less and server-less build
test_help_no_gui_no_server:
  extends:
    - .fdo.distribution-image@arch
  needs: [build_no_gui_no_server]
  stage: test
  script:
    # no gui option, invert result
    - bash -c "./build/src/app/umr -h | grep -F -- '--gui' && exit 1; exit 0"
    # no server option, invert result
    - bash -c "./build/src/app/umr -h | grep -F -- '--server' && exit 1; exit 0"


###
### Below instructions are only relevant for release builds
###

.release_rules:
  rules:
    - if: $CI_COMMIT_TAG # Run this job when a tag is created manually

# this is for umr.deb release
build_docker_ubuntu:
  extends:
    - .fdo.container-build@ubuntu
    - .release_rules
  stage: build_docker
  variables:
    FDO_DISTRIBUTION_PACKAGES: >
      build-essential
      cmake
      git
      libdrm-dev
      libgbm-dev
      libglew-dev
      libglx-dev
      libnanomsg-dev
      libncurses-dev
      libopengl-dev
      libpciaccess-dev
      libsdl2-dev
      llvm-11-dev
      pkgconf
      rpm
      zlib1g-dev

# this is for umr.rpm release
build_docker_fedora:
  extends:
    - .fdo.container-build@fedora
    - .release_rules
  stage: build_docker
  variables:
    FDO_DISTRIBUTION_PACKAGES: >
      SDL2-devel
      cmake
      g++
      gcc
      git
      libdrm-devel
      libffi-devel
      libgbm-devel
      libpciaccess-devel
      llvm-devel
      make
      nanomsg-devel
      ncurses-libs
      rpm-build
      zlib-devel

build_deb:
  <<: *build
  extends:
    - .fdo.distribution-image@ubuntu
    - .release_rules
  variables:
    CMAKE_EXTRA_ARGS: '-DCPACK_GENERATOR=DEB'
    MAKE_EXTRA_ARGS: 'package'
  artifacts:
    paths:
    - build/umr*.deb

build_rpm:
  <<: *build
  extends:
    - .fdo.distribution-image@fedora
    - .release_rules
  variables:
    CMAKE_EXTRA_ARGS: '-DCPACK_GENERATOR=RPM -DLLVM_DIR=/usr/lib64/llvm11/lib/cmake/llvm'
    MAKE_EXTRA_ARGS: 'package'
  artifacts:
    paths:
    - build/umr*.rpm

build_zst:
  stage: build
  extends:
    - .fdo.distribution-image@arch
    - .release_rules
  script:
    - mkdir -p build_arch
    # must exclude directory where the archive is being created
    # otherwise it will fail with 'file changed as we read it'
    - tar cvfz build_arch/umr.tar --exclude='build_arch' ./
    - cp PKGBUILD build_arch/
    # MUST run makepkg as user...
    - useradd fake_user_makepkg
    - 'chown fake_user_makepkg build_arch -R'
    - su fake_user_makepkg -c 'cd build_arch; makepkg'
    - mkdir -p build
    - cp build_arch/umr*.pkg.tar.zst build/
  artifacts:
    paths:
    - build/umr*.pkg.tar.zst

package_please:
  extends:
    - .release_rules
  # special release image, see https://docs.gitlab.com/ee/ci/yaml/#release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  needs: ['test_help', 'build_deb', 'build_rpm', 'build_zst']
  script:
    - echo 'Packaging artifacts...'
    - pwd
    - ls -lah
    - ls -lah build/
    - cp build/umr*.deb ./umr-${CI_COMMIT_TAG}-Linux.deb
    - cp build/umr*.rpm ./umr-${CI_COMMIT_TAG}-Linux.rpm
    - cp build/umr*.pkg.tar.zst ./umr-${CI_COMMIT_TAG}-Linux.pkg.tar.zst
  release:
    tag_name: $CI_COMMIT_TAG
    name: 'Release $CI_COMMIT_TAG'
    description: '
      Release created using the release-cli.


      Debian/Ubuntu:


      \`sudo apt install ./umr-${CI_COMMIT_TAG}-Linux.deb\`


      Fedora/CentOS/RHEL:


      \`sudo rpm -i ./umr-${CI_COMMIT_TAG}-Linux.rpm\`


      Arch Linux:


      \`sudo pacman -U ./umr-${CI_COMMIT_TAG}-Linux.pkg.tar.zst\`'
    assets:
      links:
        - name: 'UMR DEB release'
          url: '${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/file/umr-${CI_COMMIT_TAG}-Linux.deb'
        - name: 'UMR RPM release'
          url: '${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/file/umr-${CI_COMMIT_TAG}-Linux.rpm'
        - name: 'UMR Arch Linux release'
          url: '${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/file/umr-${CI_COMMIT_TAG}-Linux.pkg.tar.zst'

  artifacts:
    paths:
      - umr-${CI_COMMIT_TAG}-Linux.deb
      - umr-${CI_COMMIT_TAG}-Linux.rpm
      - umr-${CI_COMMIT_TAG}-Linux.pkg.tar.zst
